# Continuous Integration / Continuous Delivery

- Jenkins 2.x with master-slave configuration
- Docker private registry v2

# Deploy

- `make all`
- `docker container ls`

# Note

- Jenkins at http://{{IP_HOST}}:8081/
- Docker private registry at http://{{IP_HOST}}:5000/v2/_catalog/
- Docker registry CLI (https://github.com/andrey-pohilko/registry-cli) => `docker run --rm --network cicd_ci_net anoxis/registry-cli -r http://registry:5000 --help`

# Throubleshooting

- Sometimes if host machine reboot then jenkins slave container doesn't reconnect to jenkins master. Delete slave node from Jenkins Nodes configuration and refresh pages